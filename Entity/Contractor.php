<?php

namespace NW\WebService\References\Operations\Notification;

class Contractor
{
    public const TYPE_CUSTOMER = 0;

    public function __construct(
        protected int $id,
        protected int $type,
        protected string $name,
        protected ?Seller $seller = null,
        protected string $email,
        protected bool $mobile)
    {
    }

    public function getFullName(): string
    {
        return $this->name . ' ' . $this->id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSeller(): ?Seller
    {
        return $this->seller;
    }

    public function isMobile(): bool
    {
        return $this->mobile;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}

