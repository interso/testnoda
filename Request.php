<?php

namespace NW\WebService\References\Operations\Notification;

final readonly class Request
{

    public function __construct(private array $params = [])
    {
    }

    public function getParameter(string $name, $default = null): mixed
    {
        return $this->params[$name] ?? $default;
    }

    public function isParameterExist(string $name): bool
    {
        return array_key_exists($name, $this->params);
    }
}