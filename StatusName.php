<?php

namespace NW\WebService\References\Operations\Notification;

enum StatusName: string
{
    case COMPLETED = "Completed";
    case PENDING = "Pending";
    case REJECTED = "Rejected";
}
