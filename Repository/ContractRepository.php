<?php

namespace NW\WebService\References\Operations\Notification\Repository;

use NW\WebService\References\Operations\Notification\Contractor;

class ContractRepository implements ContractRepositoryInterface
{
    public function __construct()
    {
        // database, orm , etc connector
    }
    public function findOneById(int $id): ?Contractor
    {
        // request to connector and return Contractor
        throw new \RuntimeException('Need implementation');
    }
}