<?php

namespace NW\WebService\References\Operations\Notification\Repository;

use NW\WebService\References\Operations\Notification\Contractor;
use NW\WebService\References\Operations\Notification\Seller;

class ContractRepositoryFake implements ContractRepositoryInterface
{

    public function findOneById(int $id): ?Contractor
    {
        $seller = new Seller($id, Contractor::TYPE_CUSTOMER, 'Seller name');
        return new Contractor($id, Contractor::TYPE_CUSTOMER, 'Name', $seller);
    }
}