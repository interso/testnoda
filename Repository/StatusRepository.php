<?php

namespace NW\WebService\References\Operations\Notification\Repository;

use NW\WebService\References\Operations\Notification\Status;
use NW\WebService\References\Operations\Notification\StatusName;

class StatusRepository
{
    public function getStatusById(int $id): Status
    {
        $status = match($id) {
            0 => StatusName::COMPLETED,
            1 => StatusName::PENDING,
            2 => StatusName::REJECTED
        };
        return new Status($id, $status);
    }
}