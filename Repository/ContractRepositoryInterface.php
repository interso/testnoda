<?php

namespace NW\WebService\References\Operations\Notification\Repository;

use NW\WebService\References\Operations\Notification\Contractor;

interface ContractRepositoryInterface
{
    public function findOneById(int $id): ?Contractor;

}