<?php

namespace NW\WebService\References\Operations\Notification;

final readonly class Status
{
    public function __construct(public int $id, public StatusName $name)
    {
    }

    public function getName(): string
    {
        return $this->name->value;
    }
}
