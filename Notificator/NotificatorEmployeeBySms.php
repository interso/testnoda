<?php

namespace NW\WebService\References\Operations\Notification\Notificator;

use NW\WebService\References\Operations\Notification\NotificationEvents;
use NW\WebService\References\Operations\Notification\Operation;

class NotificatorEmployeeBySms implements NotificatorOperationInterface
{


    public function __construct(private readonly MessageSmsSender $messageSender)
    {
    }

    public function notify(Operation $operation, array $templateParameters): NotificationResult
    {
//        if ($operation->getNotificationType() === self::TYPE_CHANGE && !empty($data['differences']['to'])) {

        $client = $operation->getClient();
        $reseller = $operation->getReseller();

        if (!$client->isMobile()) {
            return new NotificationResult(false);
        }

        $error = '';
        $res = $this->messageSender->send(
            $reseller->getId(),
            $client->getId(),
            NotificationEvents::CHANGE_RETURN_STATUS,
            (int)$data['differences']['to'],
            $templateParameters,
            $error
        );

        $result = new NotificationResult($res);
        if (!empty($error)) {
            $result->setErrorMessage($error);
        }

        return $result;
    }
}