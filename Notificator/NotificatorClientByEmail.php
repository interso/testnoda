<?php

namespace NW\WebService\References\Operations\Notification\Notificator;

use NW\WebService\References\Operations\Notification\NotificationEvents;
use NW\WebService\References\Operations\Notification\Operation;

use NW\WebService\References\Operations\Notification\Template\Template;
use NW\WebService\References\Operations\Notification\Template\TemplateEngineInterface;

use function NW\WebService\References\Operations\Notification\getEmailsByPermit;
use function NW\WebService\References\Operations\Notification\getResellerEmailFrom;

class NotificatorClientByEmail implements NotificatorOperationInterface
{

    public function __construct(private readonly MessageClientSender $messageSender, private readonly TemplateEngineInterface $templateEngine)
    {
    }

    public function notify(Operation $operation, array $templateParameters): NotificationResult
    {
        $reseller = $operation->getReseller();
        $client = $operation->getClient();

//        if ($operation->getNotificationType() === self::TYPE_CHANGE && !empty($data['differences']['to'])) {
        $emailFrom = getResellerEmailFrom($reseller->getId());
        if (empty($emailFrom) || empty($client->getEmail())) {
            return new NotificationResult(false);
        }

        $message = [];
        $message['emailFrom'] = $emailFrom;
        $message['emailTo'] = $client->getEmail();

        $subjectTemplate = new Template('complaintClientEmailSubject', $templateParameters);
        $message['subject'] = $this->templateEngine->resolve($subjectTemplate);

        $messageTemplate = new Template('complaintClientEmailBody', $templateParameters);
        $message['message'] = $this->templateEngine->resolve($messageTemplate);

        $this->messageSender->sendMessage(
            [$message],
            $reseller->getId(),
            $client->getId(),
            NotificationEvents::CHANGE_RETURN_STATUS,
            (int)$data['differences']['to']);

        return new NotificationResult(true);
    }
}