<?php

namespace NW\WebService\References\Operations\Notification\Notificator;

use NW\WebService\References\Operations\Notification\NotificationEvents;
use NW\WebService\References\Operations\Notification\Operation;

use NW\WebService\References\Operations\Notification\Template\Template;
use NW\WebService\References\Operations\Notification\Template\TemplateEngineInterface;

use function NW\WebService\References\Operations\Notification\getEmailsByPermit;
use function NW\WebService\References\Operations\Notification\getResellerEmailFrom;

class NotificatorEmployeeByEmail implements NotificatorOperationInterface
{

    public function __construct(private readonly MessageSender $messageSender, private readonly TemplateEngineInterface $templateEngine)
    {
    }
    public function notify(Operation $operation, array $templateParameters): NotificationResult
    {
        $reseller = $operation->getReseller();

        $emailFrom = getResellerEmailFrom($reseller->getId());
        if (empty($emailFrom)) {
            return new NotificationResult(false);
        }

        $emails = getEmailsByPermit($reseller->getId(), 'tsGoodsReturn');
        if (!is_array($emails) || count($emails) === 0) {
            return new NotificationResult(false);
        }

        $message = [];
        $message['emailFrom'] = $emailFrom;

        $subjectTemplate = new Template('complaintEmployeeEmailSubject', $templateParameters);
        $message['subject'] = $this->templateEngine->resolve($subjectTemplate);

        $messageTemplate = new Template('complaintEmployeeEmailBody', $templateParameters);
        $message['message'] = $this->templateEngine->resolve($messageTemplate);

        foreach ($emails as $email) {
            $message['emailTo'] = $email;
            $this->messageSender->sendMessage([$message], $reseller->getId(), NotificationEvents::CHANGE_RETURN_STATUS);
        }

        return new NotificationResult(true);
    }
}