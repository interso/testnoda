<?php

namespace NW\WebService\References\Operations\Notification\Notificator;

use NW\WebService\References\Operations\Notification\Operation;

interface NotificatorOperationInterface
{
    public function notify(Operation $operation, array $templateParameters): NotificationResult;

}
