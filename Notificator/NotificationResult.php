<?php

namespace NW\WebService\References\Operations\Notification\Notificator;

final class NotificationResult
{
    public function __construct(public readonly bool $status, private string $errorMessage = '')
    {

    }
    public function setErrorMessage(string $message): void
    {
        $this->errorMessage = $message;
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

}