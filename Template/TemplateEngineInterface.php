<?php

namespace NW\WebService\References\Operations\Notification\Template;

interface TemplateEngineInterface
{
    public function resolve(Template $template): string;
}