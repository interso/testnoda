<?php

namespace NW\WebService\References\Operations\Notification\Template;

class Template
{

    public function __construct(private readonly string $name, private array $parameters = [])
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function setParameters(array $parameters = []): void
    {
        $this->parameters = $parameters;
    }

    public function setParameter(string $name, mixed $value): void
    {
        $this->parameters[$name] = $value;
    }

    public function getParameter(string $name, $default = null): mixed
    {
        return $this->parameters[$name] ?? $default;
    }
}