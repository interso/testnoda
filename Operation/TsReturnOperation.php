<?php

namespace NW\WebService\References\Operations\Notification;

use NW\WebService\References\Operations\Notification\Notificator\NotificationResult;
use NW\WebService\References\Operations\Notification\Notificator\NotificatorOperationInterface;
use NW\WebService\References\Operations\Notification\Repository\StatusRepository;

class TsReturnOperation implements ReferencesOperation
{
    private StatusRepository $statusRepository;

    public function __construct(StatusRepository $statusRepository)
    {
        $this->statusRepository = $statusRepository;
    }

    /**
     * @var array<string, NotificatorOperationInterface>
     */
    private array $notificators = [];

    protected function validate(Operation $operation): void
    {
        if (!$operation->getReseller()) {
            throw new \RuntimeException('Reseller not found!', 400);
        }

        if (!$operation->getClient() ||
            $operation->getClient()->getType() !== Contractor::TYPE_CUSTOMER ||
            $operation->getClient()->getSeller()?->getId() !== $operation->getReseller()->getId()) {
            throw new \RuntimeException('Client not found!', 400);
        }

        if (!$operation->getCreator()) {
            throw new \RuntimeException('Creator not found!', 400);
        }

        if (!$operation->getExpert()) {
            throw new \RuntimeException('Expert not found!', 400);
        }
    }

    public function addNotificator(string $notificatorName, NotificatorOperationInterface $notificator): void
    {
        $this->notificators[$notificatorName] = $notificator;
    }

    /**
     * @param Operation $operation
     * @return array<string, NotificationResult>
     * @throws \Exception
     */
    public function doOperation(Operation $operation): array
    {
        $this->validate($operation);

        $differences = $this->calculateDifferencesAsString($operation);

        $templateData = $this->prepareTemplateData($operation);
        $templateData['DIFFERENCES'] = $differences;

        $result = [];
        foreach ($this->notificators as $notificatorName => $notificator) {
            $result[$notificatorName] = $notificator->notify($operation, $templateData);
        }

        return $result;
    }

    private function calculateDifferencesAsString(Operation $operation): string
    {
        $notificationType = $operation->getNotificationType();
        $resellerId = $operation->getReseller()->getId();

        $differences = '';
        if ($notificationType === Operation::TYPE_NEW) {


            $differences = __('NewPositionAdded', null, $resellerId);
        } elseif ($notificationType === Operation::TYPE_CHANGE && !empty($data['differences'])) {
            $differences = __('PositionStatusHasChanged', [
                'FROM' => $this->statusRepository->getStatusById((int)$data['differences']['from'])?->getName(),
                'TO'   => $this->statusRepository->getStatusById((int)$data['differences']['to'])?->getName(),
            ], $resellerId);
        }

        return $differences;
    }

    private function prepareTemplateData(Operation $operation): array
    {
        $templateData = [
            'COMPLAINT_ID' => (int)$data['complaintId'],
            'COMPLAINT_NUMBER' => (string)$data['complaintNumber'],
            'CREATOR_ID' => $cr->getId(),
            'CREATOR_NAME' => $cr->getFullName(),
            'EXPERT_ID' => $et->getId(),
            'EXPERT_NAME' => $et->getFullName(),
            'CLIENT_ID' => $client->getId(),
            'CLIENT_NAME' => $client->getFullName(),
            'CONSUMPTION_ID' => (int)$data['consumptionId'],
            'CONSUMPTION_NUMBER' => (string)$data['consumptionNumber'],
            'AGREEMENT_NUMBER' => (string)$data['agreementNumber'],
            'DATE' => (string)$data['date'],
            'DIFFERENCES' => $differences,
        ];

        // Если хоть одна переменная для шаблона не задана, то не отправляем уведомления
        foreach ($templateData as $key => $tempData) {
            if (empty($tempData)) {
                throw new \Exception("Template Data ({$key}) is empty!", 500);
            }
        }

        return $templateData;
    }

}
