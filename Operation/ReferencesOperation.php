<?php

namespace NW\WebService\References\Operations\Notification;

use NW\WebService\References\Operations\Notification\Notificator\NotificationResult;
use NW\WebService\References\Operations\Notification\Notificator\NotificatorOperationInterface;

interface ReferencesOperation
{
    /**
     * @param Operation $operation
     * @return array<string, NotificationResult>
     */
    public function doOperation(Operation $operation): array;

    public function addNotificator(string $notificatorName, NotificatorOperationInterface $notificator): void;

}
