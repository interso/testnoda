<?php

namespace NW\WebService\References\Operations\Notification;

readonly class Operation
{
    public const TYPE_NEW    = 1;
    public const TYPE_CHANGE = 2;

    private ?Seller $reseller;
    private ?Contractor $client;
    private ?Employee $creator;
    private ?Employee $expert;

    public function __construct(
        private readonly Complaint $complaint,
        private readonly Consumption $consumption,
        private readonly DateTime $date)
    {
    }

//'COMPLAINT_ID'       => (int)$data['complaintId'],
//'COMPLAINT_NUMBER'   => (string)$data['complaintNumber'],
//'CREATOR_ID'         => $cr->getId(),
//'CREATOR_NAME'       => $cr->getFullName(),
//'EXPERT_ID'          => $et->getId(),
//'EXPERT_NAME'        => $et->getFullName(),
//'CLIENT_ID'          => $client->getId(),
//'CLIENT_NAME'        => $client->getFullName(),
//'CONSUMPTION_ID'     => (int)$data['consumptionId'],
//'CONSUMPTION_NUMBER' => (string)$data['consumptionNumber'],
//'AGREEMENT_NUMBER'   => (string)$data['agreementNumber'],
//'DATE'               => (string)$data['date'],
//'DIFFERENCES'        => $differences,

    public function getReseller(): Seller
    {
        return $this->reseller;
    }

    public function getClient(): Contractor
    {
        return $this->client;
    }

    public function getCreator(): Employee
    {
        return $this->creator;
    }

    public function getExpert(): Employee
    {
        return $this->expert;
    }

    public function getNotificationType(): int
    {
        return $this->notificationType;
    }

    public function setReseller(Seller $reseller): void
    {
        $this->reseller = $reseller;
    }

    public function setClient(Contractor $client): void
    {
        $this->client = $client;
    }

    public function setCreator(Employee $creator): void
    {
        $this->creator = $creator;
    }

    public function setExpert(Employee $expert): void
    {
        $this->expert = $expert;
    }


}
